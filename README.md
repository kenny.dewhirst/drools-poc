# Drools Proof of Concept

I'm having an issue with using a Drools Fact type defined in one file and used in another. IntelliJ's editors think that the type is not defined, but building and running work fine. This project will serve to be as simple a recreation of the issue as possible.

In [rule.drl](./src/main/resources/com/ftc_llc/droolspoc/rule.drl), I reference a type defined in [type.drl](src/main/resources/com/ftc_llc/droolspoc/type.drl). IntelliJ marks that type as something it cannot resolve, but when I run the main class, [App.java](./src/main/java/com/ftc_llc/droolspoc/App.java), from inside of IntelliJ, it works (prints "Hello, world") as expected. It also works when I build and run the JAR from the command line:

```sh
mvn clean install
java -jar ./target/droolspoc-1.0-SNAPSHOT-jar-with-dependencies.jar
```

So it seems like there is a false positive in the editor analysis, or that IntelliJ is resolving the types differently at time of writing than it does at compile and run time.

This project was used to demonstrate the issue behind [this IntelliJ YouTrack ticket](https://youtrack.jetbrains.com/issue/IDEA-179473).
