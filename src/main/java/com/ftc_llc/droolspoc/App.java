package com.ftc_llc.droolspoc;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

/**
 * Proof of concept application for an issue in the IntelliJ code editor
 */
public class App 
{
    public static void main( String[] args )
    {
        // Run the Drools rules
        runSession();
    }

    /**
     * Runs the business rule engine
     */
    private static void runSession(){
        // load up the knowledge base
        KieServices ks = KieServices.Factory.get();
        KieContainer kContainer = ks.getKieClasspathContainer();
        // This name matches the ksessionname of the rules knowledge base in the kmodule.xml file
        String kSessionName = "ksession-rules";
        KieSession kSession = kContainer.newKieSession(kSessionName);

        // Run the 'before' rules
        kSession.getAgenda().getAgendaGroup("before").setFocus();
        kSession.fireAllRules();

        // Run the 'main' rules
        kSession.getAgenda().getAgendaGroup("main").setFocus();
        kSession.fireAllRules();
    }
}
